﻿#include <string>
#include <vector>
#include <iostream>

using namespace std;

string join(vector<string> arr, string del) {

	string str = "";

	for (int i = 0; i < arr.size(); i++) {
		if (i == arr.size() - 1) {
			str += arr[i];
			continue;
		}
		str += arr[i] + del;
	}

	return str;
}


vector<string> split(string str, string del) {

	vector<string> result;
	string buff = "";

	if (del.length() == 0) {

		for (int i = 0; i < str.length(); i++) {
			string s(1, str[i]);
			result.push_back(s);
		}

		return result;
	}
	else {

		for (int i = 0; i < str.length(); i++) {
			int counter = 0;
			string s(1, str[i]);
			buff += s;

			for (int k = 0; k < del.length(); k++) {
				if (str[i + k] == del[k]) {
					counter++;
					if (counter == del.length()) {
						result.push_back(buff.substr(0, buff.length() - 1));
						buff = "";
						i += k;
					}
				}
			}

			if (i == str.length() - 1) {
				result.push_back(buff);
			}
		}

		return result;
	}
}



int main()
{
	vector<string> splited = split("hello world", " ");
	string joined = join(splited, " ");
	return 0;
}

