import netifaces
import os
from sys import platform, argv
from datetime import datetime

is_termux = False

if len(argv) > 1:
    is_termux = True
print(argv[0])
delimiter = ""
if platform == "win32":
    delimiter = "\\" 
else:
    delimiter = "/"   

path = os.path.dirname(os.path.abspath(__file__)) 
if is_termux:
    interface = netifaces.gateways()[2][0][1]
else:
    i = netifaces.gateways()["default"][netifaces.AF_INET][1]
info = netifaces.ifaddresses(i)[netifaces.AF_INET][0]
ip = info["addr"]
mask = info["netmask"]
broadcast = info["broadcast"]
hello_cpp = ip + ";" + mask + ";" + broadcast + ";"

with open(path + delimiter + "inet_info.txt", "w") as file:
    file.write(hello_cpp)

# log
now = datetime.now()
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
log = hello_cpp + "\t" + dt_string + "\n"
with open(path + delimiter + "log.txt", "a") as file:
    file.write(log)

# if file log.txt have big size, then delete it

